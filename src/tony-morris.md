Tony Morris
===========

----

>  Software Engineer, specialising in applying Functional Programming to achieve high-assurance, rapid software development

> <cv@tmorris.net> • +61 408711099 •
> Brisbane, Australia

> [github projects](https://github.com/tonymorris) • [gitlab projects](https://gitlab.com/tonymorris) • [presentations](https://presentations.tmorris.net/)

----

Professional Experience
-----------------------

2023-*present*
:   **Senior Software Engineer**
:   QANTAS

Maintaining and implementing new features for the QANTAS Hotels and QANTAS Holidays backend.

2022-2023
:   **Senior Software Engineer**
:   Boozebud

Maintaining and implementing new features on the Boozebud online retail presence, using the Scala programming language.

2020-2022
:   **Principal Software Consultant**
:   Simple Machines

Providing expert software and data engineering outcomes to clients working in a variety of domains. Technologies implemented include:

* Apache Pulsar for video streaming on the Chatroullete platform
* Kafka for integrating and streaming multiple database sources for IAG insurance
* Typescript/HTML for implementing a front-end dashboard to Kafka data streams
* Utilising the Scala programming language to take advantage of functional programming principles
* Management of container software applications e.g. using Kubernetes and Docker
* Apache Spark for distribution of large scale data pipelines
* Amazon Web Services (AWS)
  * S3 storage
  * Copilot containers
  * EC2 (elastic compute)
  * Redshift data warehousing

2019-*present*
:   **Senior RA Flight Instructor & Grade 3 Flight Instructor**
:   Flightscope Aviation

Providing ground and in-flight instruction to ab initio students in
single-engine, 3-axis aeroplanes. Based at Archerfield Airport, Brisbane.

2020-2021
:   **Course Coordinator, Lecturer**
:   School of ITEE, University of Queensland

Course Coordinator, Lecturer and Tutor for COMP3400 Functional & Logic Programming.
Semester 1, 2020.

2015-2020
:   **Functional Programming Team Leader**
:   Queensland FP Lab, Data61, CSIRO

Managing a team of 10 software developers, with various software projects that utilise functional programming to achieve the objectives. This role as team leader of the [Queensland Functional Programming Lab](https://qfpl.io/) involves the delivery of impact-based software solutions to industry and government. Examples include:

* the legislated Consumer Data Rights for the Australian government (ACCC) in an open banking solution. This project produces a consistent API (e.g. REST) for consumers to interact with their banking provider.
* delivery of software development education programs to industry and government partners
* development of open-source software solutions with a view to help industry achieve specific outcomes
* the creation of the open-source [hpython](https://github.com/qfpl/hpython) library for an Abstract Syntax Tree for programming in python. The library takes advantage of lenses for manipulating and analysing python programs.

2012-2015
:   **Senior Software Engineer**
:   National ICT Australia

Working on various software projects under the Australian federal government technology research centre NICTA. Some of these software projects become their own companies, with independent revenue. Some others continue on as open source software libraries that are used throughout various industry and Australian government departments.

2010-2012
:   **Senior Software Engineer**
:   Ephox Pty Ltd

Working with Haskell (primarily), Scala, Java and Javascript to solve business problems for a product company specialising in rich editors. These projects include the TinyMCE text editor, which appears in most web forms for rich text editing, and WebRadar for content analytics and specifically, to integrate with related IBM software solutions.

Ephox has since changed names and is now called Tiny.

2009-2010
:   **Senior Software Engineer**
:   Eyecon Pty Ltd

Implementing gaming solutions using the Java/J2EE platform. The gaming solutions include online gaming platforms and physical gaming machines. Much of the work here involved not just implementation, but testing and assuring compliance with regulatory requirements.

2008-2009
:   **Senior Software Engineer**
:   Paycorp Payment Solutions Pty Ltd

Implementing payment business solutions using the Scala Programming Language and related programming techniques. Paycorp implements payment solutions for Australian retail merchants for seamless payment acceptance. This role required  proficiency in ensuring a timely and correct software delivery and with fast adaptation to changing requirements.

2006-2008
:   **Senior Software Engineer**
:   Workingmouse Pty Ltd

A software development consultancy, primarily using the Java programming language to implement solutions. In this role, I did not directly liase with clients, but provided technical software development support to the different teams deployed to different client sites.

2002-2006
:   **Software Engineer**
:   IBM Corporation

Working under the Tivoli security brand implementing the IBM Tivoli Risk Manager adapter for IBM Tivoli Access Manager targeting seven operating system platforms.

Technical Skills
----------------

* Functional Programming
  * Scala
  * Haskell
  * F#
  * Agda
  * Idris
* Object-Oriented Programming
  * Java
  * C#
* Python
* Ruby
* JavaScript
* TypeScript
* Bash
* Zsh
* Platform as a Service (PaaS)
  * Docker
  * Kubernetes
  * Amazon Web Services (AWS)
* Software as a Service (SaaS)
* Software frameworks
  * Apache Spark
  * Akka
  * Apache Kafka
  * Spring Framework
  * Scalaz

Education
---------

2022
:   Grade 3 Flight Instructor Rating, Flightscope Aviation

2022
:   Commercial Pilot Licence (Aeroplane), Flightscope Aviation

2021
:   RA Senior Flight Instructor Rating (3-axis), Flightscope Aviation

2021
:   Tailwheel design feature endorsement, Flightscope Aviation

2021
:   Aviation English Language Proficiency Assessor (AELP6 & GELP), Civil Aviation Safety Authority

2020
:   Retractable undercarriage design feature endorsement, Flightscope Aviation

2020
:   Night VFR Rating, Flightscope Aviation

2019
:   RA Flight Instructor Rating (3-axis), Flightscope Aviation

2019
:   Manual Propellor Pitch Control (MPPC) endorsement, Flightscope Aviation

2018
:   Remote Pilot Licence (RePL), Civil Aviation Safety Authority

2018
:   Recreational Pilot Certificate (3-axis), Flightscope Aviation

2017
:   Private Pilot Licence (aeroplane category), Pathfinder Aviation

2016
:   Recreational Pilot Licence (aeroplane category), Flight One

2011
:   World Squash Federation accredited referee

2005
:   Sun Certified Programmer for the Java 2 Platform 5.0

2003
:   Sun Certified Developer for the Java Platform 1.4

2002
:   Sun Certified Programmer for the Java 2 Platform 1.4

2001
:   Griffith Award for Academic Excellence for studies in the Bachelor of Information Technology, Griffith University

2001
:   Bachelor of Information Technology, Griffith University

1996
:   Certificate 3 in Information Technology

1995
:   Certificate 4 in Psychology

References
----------

*available upon request*

----

> <cv@tmorris.net> • +61 408711099 •
> Brisbane, Australia

> [github projects](https://github.com/tonymorris) • [gitlab projects](https://gitlab.com/tonymorris) • [presentations](https://presentations.tmorris.net/)

<div style="color:#808080; font-style:italic; font-size:0.4em; text-align:right">
  Last updated: <b>${COMMIT_TIME}</b>

  Revision: <b>${CI_COMMIT_SHA}</b>
</div>
